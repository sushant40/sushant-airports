$(document).ready(function(){
	$("#loginBtn").on('click', doLogin);
	
	$("#countries, #airportCode, #airportType").on('change keyup', reloadData);
});

function doLogin(){
	$.ajax({
		url : '/api/login'
	}).done(function( data, textStatus, jqXHR ) {
		$("#loginBtn").text("LOGGED IN!");
		$(".responseCode").text('LOGGED IN!');
	})
}

var delay = undefined;
function reloadData(){
	if (delay != undefined)
	{
		clearTimeout(delay);
	}
	
	$("#responsePanel").html('');
	delay = setTimeout(function(){
		var data = {};

		if ($("#countries").val().trim().length > 0)
		{
			data.country = $("#countries").val().trim().split(',');
		}
		if ($("#airportCode").val().trim().length > 0)
		{
			data.airportCode = $("#airportCode").val().trim();
		}
		if ($("#airportType option:selected").val().trim().length > 0)
		{
			data.airportType = $("#airportType option:selected").val().trim();
		}
		
		$.ajax({
			url : '/api/search',
			data : data,
			traditional : true
		}).fail(function( jqXHR, textStatus, errorThrown ) {
			$(".responseCode").text('ERROR: '+jqXHR.statusCode().status + " "+errorThrown);
		}).done(function( data, textStatus, jqXHR ) {
			$("#responsePanel").text(JSON.stringify(data, null, ' '));
			$(".responseCode").text(data.length +' matches');
		});
	},150);
}