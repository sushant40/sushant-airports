
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sushant</title>

<!-- Bootstrap core CSS -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="index.css" rel="stylesheet">
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Sushant</a>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						Login <span class="badge">/api/login</span>
					</div>
					<div class="panel-body">
						<button class="btn btn-info btn-block" id="loginBtn">Login</button>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-warning">
					<div class="panel-heading">
						Search 
						<span class="badge">/api/search</span>
					</div>
					<div class="panel-body">
						<form>
							<div class="form-group">
								<label for="countries">Countries (comma separated)</label> <input
									type="text" class="form-control" id="countries"
									placeholder="countries">
							</div>
							<div class="form-group">
								<label for="airportCode">Airport Code</label> <input
									type="text" class="form-control" id="airportCode"
									placeholder="airport code">
							</div>
							<div class="form-group">
								<label for="airportType">Airport Type</label>
								<select class="form-control" id="airportType">
									<option></option>
									<option value="international">international</option>
									<option value="domestic">domestic</option>
								</select>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
					Response
					<span class="badge responseCode">?</span>
					</div>
					<pre class="panel-body" id="responsePanel"></pre>
				</div>
			</div>
		</div>

	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"
		integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="script.js"></script>
</body>
</html>
