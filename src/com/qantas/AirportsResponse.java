package com.qantas;

import java.util.List;

public class AirportsResponse
{
	private List<Airport> airports;

	public List<Airport> getAirports()
	{
		return airports;
	}

	public void setAirports(List<Airport> airports)
	{
		this.airports = airports;
	}

	@Override
	public String toString()
	{
		return "AirportsResponse [airports=" + airports.size() + "]";
	}
}
