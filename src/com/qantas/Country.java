package com.qantas;

import com.google.gson.annotations.SerializedName;

public class Country
{
	private String code;

	@SerializedName("display_name")
	private String displayName;

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Country)
		{
			Country other = (Country) obj;
			return code.equals(other);
		}
		return false;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
}
