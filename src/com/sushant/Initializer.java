package com.sushant;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class Initializer
 */
public class Initializer extends HttpServlet
{
	private static final Logger logger = Logger.getLogger(Initializer.class);
	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig config) throws ServletException
	{
		logger.info("Data init starting");

		AirportsService.sharedInstance();

		logger.info("Data init ending");
	}
}
