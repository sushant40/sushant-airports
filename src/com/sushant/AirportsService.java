package com.sushant;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.qantas.Airport;
import com.qantas.AirportsResponse;

public class AirportsService
{
	private static final String WS_URL = "https://www.qantas.com.au/api/airports";

	private static Logger logger = Logger.getLogger(AirportsService.class);

	private static AirportsService instance;
	private static Object _sharedLock_ = new Object();

	List<Airport> airports;

	public static void clearInstance()
	{
		instance = null;
	}

	public static AirportsService createInstance(List<Airport> airports)
	{
		return new AirportsService(airports);
	}

	public static AirportsService remoteWSInstance()
	{
		try
		{
			List<Airport> airports = readFeed();
			return createInstance(airports);
		}
		catch (Exception e)
		{
			logger.fatal("Oh NO!", e);
		}

		return null;
	}

	public static AirportsService sharedInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = remoteWSInstance();
				}
			}
		}
		return instance;
	}

	private AirportsService(List<Airport> airports)
	{
		logger.info("Creating singleton of class: " + getClass().getName());
		this.airports = airports;
	}

	private static List<Airport> readFeed() throws IOException
	{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(WS_URL);
		CloseableHttpResponse response = httpclient.execute(httpGet);

		// new Gson().fromJson(, String.class)
		HttpEntity entity = response.getEntity();

		AirportsResponse airportsResponse = new Gson().fromJson(EntityUtils.toString(entity), AirportsResponse.class);

		logger.info(airportsResponse);
		return airportsResponse.getAirports();
	}

	public Set<Airport> search(String[] countries, String airportCode, AirportType airportType)
	{
		boolean checkAirportCode = StringUtils.isNotBlank(airportCode);
		boolean checkAirportType = airportType != null;
		boolean checkCountries = countries != null;

		Set<Airport> matches = new HashSet<>();
		for (Airport airport : airports)
		{
			if (checkAirportCode
					&& !airportCode.equalsIgnoreCase(airport.getCode()))
			{
				continue;
			}

			//AND
			if (checkAirportType
					&& !airportType.matches(airport))
			{
				continue;
			}

			//AND
			if (checkCountries)
			{
				boolean matchedCountry = false;
				for (String searchCountry : countries)
				{
					String airportCountryName = null;
					try
					{
						airportCountryName = airport.getCountry().getDisplayName();
					}
					catch (Exception e)
					{
						logger.warn("Bad data in feed", e);
					}
					if (StringUtils.isNotBlank(airportCountryName)
							&& StringUtils.containsIgnoreCase(airportCountryName, searchCountry))
					{
						matchedCountry = true;
						break;
					}
				}
				if (!matchedCountry)
				{
					continue;
				}
			}

			matches.add(airport);
		}

		logger.info("Matches found: " + matches.size());
		return matches;
	}

	public List<Airport> getAirports()
	{
		return airports;
	}
}
