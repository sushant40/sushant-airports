package com.sushant;

import com.qantas.Airport;

public enum AirportType
{
	//@formatter:off
	DOMESTIC("domestic"),
	INTERNATIONAL("international");

	public final String type;

	private AirportType(String type)
	{
		this.type = type;
	}

	public static AirportType fromString(String type)
	{
		for (AirportType t : values())
		{
			if (t.type.equalsIgnoreCase(type))
			{
				return t;
			}
		}
		return null;
	}

	public boolean matches(Airport airport)
	{
		if (airport.isInternationalAirport() && this == AirportType.INTERNATIONAL)
		{
			return true;
		}
		else if (airport.isRegionalAirport() && this == AirportType.DOMESTIC)
		{
			return true;
		}
		return false;
	}
}
