package com.sushant.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.sushant.controllers.AuthenticationController;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/api")
public class LoginFilter implements Filter
{
	private static Logger logger = Logger.getLogger(LoginFilter.class);

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		boolean loggedIn = Boolean.TRUE.equals(req.getSession().getAttribute(AuthenticationController.LOGGED_IN));

		logger.info(req.getRequestURI());
		logger.info("loggedIn=" + loggedIn);

		if (isExempt(req.getRequestURI()) || loggedIn)
		{
			chain.doFilter(request, response);
		}
		else
		{
			resp.setStatus(403);//unauthorised
		}
	}

	private boolean isExempt(String uri)
	{
		return "/api/login".equals(uri);
	}

	@Override
	public void destroy()
	{
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
	}
}
