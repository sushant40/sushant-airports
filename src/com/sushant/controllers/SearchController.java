package com.sushant.controllers;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qantas.Airport;
import com.sushant.AirportType;
import com.sushant.AirportsService;

@Controller
public class SearchController
{
	@RequestMapping("/search")
	@ResponseBody
	public Set<Airport> login(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam(name = "country", required = false) String[] countries, //one or more countries
			@RequestParam(required = false) String airportCode, //exact match
			@RequestParam(name = "airportType", required = false) String type) //domestic or international
	{
		AirportType airportType = AirportType.fromString(type);
		return AirportsService.sharedInstance().search(countries, airportCode, airportType);
	}
}
