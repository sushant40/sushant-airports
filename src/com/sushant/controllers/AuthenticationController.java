package com.sushant.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AuthenticationController
{

	public static final String LOGGED_IN = "LOGGED_IN";

	@RequestMapping("/login")
	@ResponseBody
	public String login(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session)
	{
		session.setAttribute(LOGGED_IN, Boolean.TRUE);
		return "OK";
	}
}
