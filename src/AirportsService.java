import org.apache.log4j.Logger;

public class AirportsService
{
	private static Logger logger = Logger.getLogger(AirportsService.class);

	private static AirportsService instance;
	private static Object _sharedLock_ = new Object();

	public static AirportsService sharedInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = new AirportsService();
				}
			}
		}
		return instance;
	}

	private AirportsService()
	{
		logger.info("Creating singleton of class: " + getClass().getName());
	}
}
