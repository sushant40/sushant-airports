package com.sushant.test.feed;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.qantas.Airport;
import com.sushant.AirportType;
import com.sushant.AirportsService;

public class FeedReadingTest
{
	private AirportsService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		AirportsService.sharedInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
		AirportsService.clearInstance();
	}

	@Before
	public void setUp() throws Exception
	{
		service = AirportsService.sharedInstance();
		assertTrue("Expecting to have airports in memory", service.getAirports().size() > 0);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void emptySearch()
	{
		List<Airport> airports = new LinkedList<>(service.search(null, null, null));
		assertTrue("Expecting to find airports", airports.size() > 0);
		assertEquals("Should  match", airports.size(), service.getAirports().size());
	}

	@Test
	public void searchForAirportByCode()
	{
		List<Airport> airports = new LinkedList<>(service.search(null, "SYD", null));
		assertEquals("Expecting to find Sydney Airport(SYD)", 1, airports.size());

		Airport airport = airports.get(0);
		assertEquals("SYD", airport.getCode().toUpperCase());
	}

	@Test
	public void searchForInvalidAirportByCode()
	{
		List<Airport> airports = new LinkedList<>(service.search(null, "ZZZ", null));
		assertEquals("Expecting to find no airport", 0, airports.size());
	}

	@Test
	public void searchForInvalidCountry()
	{
		List<Airport> airports = new LinkedList<>(service.search(new String[] { "XXX" }, null, null));
		assertEquals("Expecting to find no airport", 0, airports.size());
	}

	@Test
	public void searchForAirportByMultiCountry()
	{
		List<Airport> airports = new LinkedList<>(service.search(new String[] { "Fiji", "Singapore" }, null, null));
		assertTrue("Expecting to find airports", airports.size() > 0);
		for (Airport airport : airports)
		{
			boolean isFiji = "Fiji".equalsIgnoreCase(airport.getCountry().getDisplayName());
			boolean isSingapore = "Singapore".equalsIgnoreCase(airport.getCountry().getDisplayName());
			assertTrue("Airport should be in Fiji or Singapore", isFiji || isSingapore);
		}
	}

	@Test
	public void searchForAirportByTypeInternational()
	{
		List<Airport> airports = new LinkedList<>(service.search(null, null, AirportType.INTERNATIONAL));
		assertTrue("Expecting to find airports", airports.size() > 0);
		for (Airport airport : airports)
		{
			assertTrue("Should be International", airport.isInternationalAirport());
		}
	}

	@Test
	public void searchForAirportByTypeDomestic()
	{
		List<Airport> airports = new LinkedList<>(service.search(null, null, AirportType.DOMESTIC));
		assertTrue("Expecting to find airports", airports.size() > 0);
		for (Airport airport : airports)
		{
			assertTrue("Should be Domestic", airport.isRegionalAirport());
		}
	}

	@Test
	public void searchForAirportByCountryType()
	{
		List<Airport> airports = new LinkedList<>(service.search(new String[] { "Australia" }, null, AirportType.INTERNATIONAL));
		assertTrue("Expecting to find airports", airports.size() > 0);
		for (Airport airport : airports)
		{
			assertTrue("Should be International", airport.isInternationalAirport());
			assertTrue("Should be In Australia", "Australia".equalsIgnoreCase(airport.getCountry().getDisplayName()));
		}
	}
}
