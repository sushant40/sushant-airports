package com.sushant.test.feed;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.qantas.Airport;
import com.sushant.AirportsService;

@RunWith(value = Parameterized.class)
public class FeedReadingCountryParameterizedTest
{
	private AirportsService service;
	private String searchCountry;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		AirportsService.sharedInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
		AirportsService.clearInstance();
	}

	@Before
	public void setUp() throws Exception
	{
		service = AirportsService.sharedInstance();
	}

	@After
	public void tearDown() throws Exception
	{
	}

	public FeedReadingCountryParameterizedTest(String searchCountry)
	{
		this.searchCountry = searchCountry.toUpperCase();
	}

	@Parameters(name = "{index}: searchForAirportByCountry({0})")
	public static Collection<Object[]> data()
	{
		return Arrays.asList(new Object[][] {
				{ "Fiji" },
				{ "Singapore" },
				{ "Australia" }
		});
	}

	@Test
	public void searchForAirportByCountry()
	{
		List<Airport> airports = new LinkedList<>(service.search(new String[] { searchCountry }, null, null));
		assertTrue("Expecting to find airports", airports.size() > 0);
		for (Airport airport : airports)
		{
			assertEquals(searchCountry, airport.getCountry().getDisplayName().toUpperCase());
		}
	}
}
